% Matlab R2019b code to demontrate FFT ability to remove noise from a
% signal
% licence: GPL 2.0
% author: Jacek Pawlyta
% date: 21.01.2020
% version: 2.0
% changes: new serial interface of Matlab used



clear; 

% initialise serial port for matlab (Linux users please make sure you gave
% write permissions to /run/lock directory STRANGE!!)
mySerialPort = serialport("/dev/ttyUSB0",57600); % define serial port

% endless loop to read data line from serial port and print it in Matlab
% console
x=1;
while x==1;
    myData = readline(mySerialPort);
    disp(myData)
end;

