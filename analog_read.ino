/*
  Arduino program
  
  Analog Read
  
  Read data from 10bits analog/digital converter connected to A0 pin and print it to serial port
*/

int analogPin = A0;    // pin A1 as an analog input
int digitalValue = 0;  // data read from analogPin

//setting up serial port speed
void setup() {
  Serial.begin(57600);
  Serial.println("Ready");
}

//main loop
void loop() {
  digitalValue = analogRead(analogPin);    
  Serial.println(digitalValue);
  delay(1000);  
}
